export function hablarConI(strInicial) {
//throw new Error('Not implemented'); 
    let strChange = []; 
    let strFinal;
    let strLength = strInicial.length;
    const regVocal = /[aeiouAEIOUáéíóúÁÉÍÓÚ]/;
    const conAcentoMinus = /[áéíóú]/;
    const conAcentoMayus = /[ÁÉÍÓÚ]/;
    const sinAcentoMayus = /[AEIOU]/;
    const sinAcentoMinus = /[aeiou]/; 
    
    
    for(let i = 0 ; i < strLength; i++){
         if(regVocal.test(strInicial[i])){
              function changeVocal (vocal) {
                   if(conAcentoMinus.test(vocal)){
                        return vocal.replace(vocal,'í');
                    }else if (conAcentoMayus.test(vocal)){
                         return vocal.replace(vocal,'Í');
                    }else if(sinAcentoMayus.test(vocal)){
                         return vocal.replace(vocal,'I');
                    }else if(sinAcentoMinus.test(vocal)){
                         return vocal.replace(vocal,'i');
                    }
               }
               
               strChange.push(changeVocal(strInicial[i]));
               strFinal = strChange.join('');
          }else{
               strChange.push(strInicial[i]);
          }
     } 

     return strFinal;
     console.log(strFinal);
     console.assert(regVocal.test(strInicial) , 'Has introducido una palabra no válida');
}


export function contarA(strEntrada) { 
     //throw new Error('Not implemented');
     const vocalMinus ='a';
     const vocalMayus = 'A';
     let sumaOcurrencias = 0;
     let ocurrenciaStrEntrada = strEntrada.indexOf(vocalMinus);
         
     
     while ( ocurrenciaStrEntrada != -1) {
          sumaOcurrencias++;
          ocurrenciaStrEntrada = strEntrada.indexOf(vocalMinus, ocurrenciaStrEntrada + 1);
     }
     
     console.log(sumaOcurrencias);
}

contarA('hala');
export function esPalindromo() { throw new Error('Not implemented'); }
export function esPrimo() { throw new Error('Not implemented'); }